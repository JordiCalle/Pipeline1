#// Crear el directorio Salida.
mkdir -p OtraSalida

#// Escribir un archivo el cual necesito que sea archivado.
#//writeFile file: "OtraSalida/ArchivoGenerado1.txt", text: "Contenido del archivo 1 generado."

#// Escribir un archivo el cual no necesito que sea archivado.
#//writeFile file: "OtraSalida/ArchivoGenerado2.md", text: "Contenido del archivo 2 generado."